<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<?php
    require_once (APPPATH.'views/includes/head.php');

  ?>
<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
  <?php
    require_once (APPPATH.'views/includes/header.php');
    require_once (APPPATH.'views/includes/sidebar.php');
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>

    <!-- Main content -->
    <section class="content">

            <!-- Small boxes (Stat box) -->
            <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?php echo $all_clients; ?></h3>

              <p>Clientes totales</p>
            </div>
            <div class="icon">
              <i class="fa fa-fw fa-group"></i>
            </div>
            <div class="small-box-footer"></div>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box  -->
          
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php echo $active_clients; ?></h3>
              <!-- <sup style="font-size: 20px">%</sup> -->

              <p>Clientes activos</p>
            </div>
            <div class="icon">
              <i class="fa fa-fw fa-check-square-o"></i>
            </div>
            <div class="small-box-footer"></div>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><?php echo $suspended_clients; ?></h3>

              <p>Clientes suspendidos</p>
            </div>
            <div class="icon">
              <i class="fa fa-fw fa-minus-circle"></i>
            </div>
            <div class="small-box-footer"></div>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>3</h3>

              <p>Facturas inpagas</p>
            </div>
            <div class="icon">
              <i class="fa fa-fw fa-file-excel-o"></i>
            </div>
            <div class="small-box-footer"></div>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->

    </section>

      <!-- Main row -->
  </div>
  <!-- /.content-wrapper -->

  <?php
    require_once (APPPATH.'views/includes/footer.php');
  ?>

<!-- jQuery 3 -->
<script src="assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="assets/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="assets/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="assets/dist/js/demo.js"></script>
<!-- ChartJS -->
<script src="assets/bower_components/chart.js/Chart.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="assets/dist/js/pages/dashboard2.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="assets/dist/js/demo.js"></script>
<script>
  $(document).ready(function () {
    $('.sidebar-menu').tree()
  })
</script>
</body>
</html>
