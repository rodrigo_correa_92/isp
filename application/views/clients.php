<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<?php
    require_once (APPPATH.'views/includes/head.php');

  ?>
<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
  <?php
    require_once (APPPATH.'views/includes/header.php');
    require_once (APPPATH.'views/includes/sidebar.php');
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Clientes</h3>
            </div>
            &nbsp;
            <div class="btn-group dt-btns">
                <button type="button" class="btn btn-sm btn-default" data-toggle="modal" data-target="#modal-default"><i class="fa fa-plus"></i> Nuevo</button>
                <button type="button" class="btn btn-sm btn-default" onclick="suspend_client()"><i class="fa fa-exchange fa-rotate-90"></i> Suspender</button>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th class="all dt-center sorting_disabled" data-orderable="false" style="padding-right: 0px !important; width: 14px;" rowspan="1" colspan="1" aria-label=""><input type="checkbox" style="font-size: 19px;" class="allsuspendido" onclick="selectallactivos();"></th>
                  <th>Cliente</th>
                  <th>DNI</th>
                  <th>Telefono</th>
                  <th>Estado</th>
                  <th>Acciones</th>

                </tr>
                </thead>
                <tbody>
    <?php	foreach ($clients as $client){
      if($client->status == 1){
        $status = "<small class='label center bg-green'>activo</small>";
      }else{
        $status = "<small class='label center bg-red'>suspendido</small>";
      }
		    echo "<tr>
                  <td><input type='checkbox' style='font-size: 19px;' name='slactivo[]'></td>
                  <td>".$client->name."</td>
                  <td>".$client->id_card."
                  </td>
                  <td>".$client->phone."</td>
                  <td>".$status."</td>
                  <td><a href='#' class='btn btn-xs btn-default' data-toggle='tooltip' data-placement='top' data-original-title='Editar cliente'><span class='glyphicon glyphicon glyphicon-edit'></span></a>
                  <button type='button' class='btn btn-xs btn-default bt-delete css-tooltip' data-toggle='tooltip' data-placement='top' data-original-title='Borrar cliente'><span class='glyphicon glyphicon-trash'></span></button></td>
                </tr>";
		} ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <div class="modal fade" id="modal-default">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"><i class="fa fa-user-plus"></i> Nuevo cliente</h4>
        </div>
        <div class="modal-body">
         <!-- general form elements -->
            <!-- form start -->
            <form role="form">
              <div class="box-body">
                <div class="form-group">
                  <label for="Name">Nombre y apellido</label>
                  <input type="text" class="form-control" id="Name" placeholder="Ingresar nombre y apellido">
                </div>
                <div class="form-group">
                  <label for="IdCard">DNI</label>
                  <input type="number" class="form-control" id="IdCard" placeholder="Ingresar nro de documento">
                </div>
                <div class="form-group">
                  <label for="Phone">Telefono</label>
                  <input type="number" class="form-control" id="Phone" placeholder="Ingresar telefono">
                </div>
                <div class="form-group">
                  <label for="Email">Email</label>
                  <input type="text" class="form-control" id="Email" placeholder="Ingresar e-mail">
                </div>
                <div class="form-group">
                  <label for="Address">Dirección</label>
                  <input type="text" class="form-control" id="Address" placeholder="Ingresar dirección">
                </div>
                 <!-- Date dd/mm/yyyy -->
              <div class="form-group">
                <label>Fecha de nacimiento</label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control" data-inputmask="'alias': 'yyyy-mm-dd'" data-mask id="BirthDate">
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->
            </form>
          </div>
          <!-- /.box -->
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
          <button type="button" class="btn btn-primary" id="addClient">Añadir cliente</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
  <?php
    require_once (APPPATH.'views/includes/footer.php');
  ?>

<!-- jQuery 3 -->
<script src="assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="assets/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="assets/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="assets/dist/js/demo.js"></script>
<!-- InputMask -->
<script src="assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script>
  $(document).ready(function () {
    $('.sidebar-menu').tree()
  })
</script>
<script>
$('#addClient').on('click',function(e) 
  {
    //alert("AJAX upload & refresh or show error alert")
    //AJAX upload & refresh or show error alert
    e.preventDefault();
		var Name = $('#Name').val();
		var IdCard = $('#IdCard').val();
    var Phone = $('#Phone').val();
    var Email = $('#Email').val();
    var Address = $('#Address').val();
		var BirthDate = $('#BirthDate').val();
       $.ajax({
            url : 'api/add_client',

            data : {Name:Name, IdCard:IdCard, Phone:Phone, Email:Email, Address:Address, BirthDate:BirthDate},

            type : 'POST',

            dataType : 'json',

            success : function(json) {
              if (json.error == 0) {
                //Correct add
                location.reload();
                
              }else{
                //Wrong add
                alert(json.text);
                
              }
            }
       });
  })
  function suspend_client(){
    alert("AJAX upload & refresh or show error alert")
    //AJAX upload & refresh or show error alert
  }
  $(function () {
    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' });
    $('[data-mask]').inputmask();
  })
</script>
</body>
</html>
