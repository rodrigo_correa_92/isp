<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<?php
    require_once (APPPATH.'views/includes/head.php');

  ?>
<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
  <?php
    require_once (APPPATH.'views/includes/header.php');
    require_once (APPPATH.'views/includes/sidebar.php');
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Usuarios</h3>
            </div>
            &nbsp;
            <div class="btn-group dt-btns">
                <button type="button" class="btn btn-sm btn-default" data-toggle="modal" data-target="#modal-newUser"><i class="fa fa-plus"></i> Nuevo</button>
                <button type="button" class="btn btn-sm btn-default" onclick="suspend_user()"><i class="fa fa-exchange fa-rotate-90"></i> Suspender</button>
                <?php if($_SESSION['permission'] == 1) { ?>
                <a class="btn btn-sm btn-default" href='export/users'><i class="fa fa-table" aria-hidden="true"></i> Exportar</a>
                <?php } ?>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

              <table id="users" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th class="all dt-center sorting_disabled" data-orderable="false" style="padding-right: 0px !important; width: 14px;" rowspan="1" colspan="1" aria-label=""><input type="checkbox" style="font-size: 19px;" class="allsuspendido" onclick="selectallactivos();"></th>
                  <th>Nombre</th>
                  <th>Email</th>
                  <th>Estado</th>
                  <th>Permiso</th>
                  <th>Acciones</th>

                </tr>
                </thead>
                <tbody>
    <?php	foreach ($users as $user){
      if($user->status == 1){
        $status = "<small class='label center bg-green'>activo</small>";
      }else{
        $status = "<small class='label center bg-red'>suspendido</small>";
      }
      if($user->permission == 1){
        $permission = "<small class='label center bg-green'>Admin</small>";
      }else{
        $permission = "<small class='label center bg-yellow'>usuario</small>";
      }
		    echo "<tr>
                  <td><input type='checkbox' style='font-size: 19px;' name='slactivo[]'></td>
                  <td>".$user->name."</td>
                  <td>".$user->email."</td>
                  <td>".$status."</td>
                  <td>".$permission."</td>
                  <td><a href='#' data-userId='".$user->id."' data-userName='".$user->name."' data-userEmail='".$user->email."' class='btn btn-xs btn-default editar_user' data-toggle='tooltip' data-placement='top' data-original-title='Editar usuario'><span class='glyphicon glyphicon glyphicon-edit'></span></a>
                  <button type='button' data-userId='".$user->id."' class='btn btn-xs btn-default bt-delete css-tooltip borrar_user' data-toggle='tooltip' data-placement='top' data-original-title='Borrar usuario'><span class='glyphicon glyphicon-trash'></span></button></td>
                </tr>";
		} ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Modal nuevo usuario -->
  <div class="modal fade" id="modal-newUser">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"><i class="fa fa-user-plus"></i> Nuevo usuario</h4>
          
        </div>
        <div class="modal-body">
        <div id="divContainerErrorMsg"></div>
         <!-- general form elements -->
            <!-- form start -->
            <form role="form">
              <div class="box-body">
              <div class="form-group">
                  <label for="Name">Nombre y apellido</label>
                  <input type="text" class="form-control" id="Name" placeholder="Ingresar nombre y apellido">
                </div>
                <div class="form-group">
                  <label for="Email">Email</label>
                  <input type="text" class="form-control" id="Email" placeholder="Ingresar e-mail">
                </div>
                
                <div class="form-group">
                  <label for="Pass">Contraseña</label>
                  <input type="password" class="form-control" id="Pass" placeholder="Ingresar contraseña">
                </div>

                <div class="form-group">
                  <label for="Pass">Repetir contraseña</label>
                  <input type="password" class="form-control" id="Pass2" placeholder="Reingresar contraseña">
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->
            </form>
          </div>
          <!-- /.box -->
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
          <button type="button" class="btn btn-primary" id="addUser">Añadir usuario</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

  <!-- Modal editar usuario -->
  <div class="modal fade" id="modal-editUser">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"><i class="fa fa-user-plus"></i> Editar usuario</h4>
          
        </div>
        <div class="modal-body">
        <div id="divContainerErrorMsgEdit"></div>
         <!-- general form elements -->
            <!-- form start -->
            <form role="form">
              <div class="box-body">
              <input type="hidden" id="editId">
              <div class="form-group">
                  <label for="Name">Nombre y apellido</label>
                  <input type="text" class="form-control" id="editName" placeholder="Ingresar nombre y apellido">
                </div>
                <div class="form-group">
                  <label for="Email">Email</label>
                  <input type="text" class="form-control" id="editEmail" placeholder="Ingresar e-mail">
                </div>

                  <input type="checkbox" id="checkEditPass">
                  <label for="checkEditPass">Editar contraseña</label>
                <div id="showEditPass" style=display:none>
                  <div class="form-group">
                    <label for="Pass">Nueva contraseña</label>
                    <input type="password" class="form-control" id="editPass" placeholder="Ingresar contraseña">
                  </div>

                  <div class="form-group">
                    <label for="Pass">Repetir contraseña</label>
                    <input type="password" class="form-control" id="editPass2" placeholder="Reingresar contraseña">
                  </div>
                </div>
                
                <!-- /.input group -->
              </div>
              <!-- /.form group -->
            </form>
          </div>
          <!-- /.box -->
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
          <button type="button" class="btn btn-primary" id="ConfirmEditUser">Editar usuario</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

  <div id='modal-deleteUser' tabindex='-1' role='dialog' class='modal fade'>
      <div class='modal-dialog'>
        <div class='modal-content'>
          <div class='modal-header'>
            <button type='button' data-dismiss='modal' aria-hidden='true' class='close'><span class='mdi mdi-close'></span></button>
          </div>
          <div class='modal-body'>
            <div class='text-center'>
              <span style="color: grey;"><i class='fas fa-trash-alt fa-7x'></i></span>
              <form action="">
                <input type="hidden" id="deleteIdUser">
              </form>
              <h3>¿Desea eliminar a este usuario?</h3>
              <h5>No podrá visualizarlo nuevamente en ningún listado</h5>
              <div class='xs-mt-50'>
                <button type='button' data-dismiss='modal' class='btn btn-space btn-default'>Volver</button>
                <button type='button' data-dismiss='modal' class='btn btn-space btn-danger' id='ConfirmDeleteUser'>Eliminar</button>
              </div>
            </div>
          </div>
          <div class='modal-footer'></div>
        </div>
      </div>
  </div>

  <?php
    require_once (APPPATH.'views/includes/footer.php');
  ?>

<!-- jQuery 3 -->
<script src="assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="assets/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="assets/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="assets/dist/js/demo.js"></script>
<!-- InputMask -->
<script src="assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script>
  $(document).ready(function () {
    $('.sidebar-menu').tree()
  })
</script>
<script>
  $('#addUser').on('click',function(e){
      
      //AJAX upload & refresh or show error alert
      e.preventDefault();
      let Name = $('#Name').val();
      let Email = $('#Email').val();
      let Pass = $('#Pass').val();
      let Pass2 = $('#Pass2').val();
      let errorMsg = '';
      let errorFlag = false;
      
      if (Name.length == 0) {
        errorMsg = 'El nombre no puede estar vacio';  
        $('#divContainerErrorMsg').append('<div id="alertDiv" class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-warning"></i> Alerta!</h4><span>'+errorMsg+'</span></div>');
        errorFlag = true;
      }
      if (Email.length == 0) {
        errorMsg = 'El E-mail no puede estar vacio';  
        $('#divContainerErrorMsg').append('<div id="alertDiv" class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-warning"></i> Alerta!</h4><span>'+errorMsg+'</span></div>');
        errorFlag = true;
      }
      if (Pass.length == 0) {
        errorMsg = 'La contraseña no puede estar vacia';  
        $('#divContainerErrorMsg').append('<div id="alertDiv" class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-warning"></i> Alerta!</h4><span>'+errorMsg+'</span></div>');
        errorFlag = true;
      }
      if (Pass.length < 8) {
        errorMsg = 'La contraseña debe tener 8 o más caracteres';  
        $('#divContainerErrorMsg').append('<div id="alertDiv" class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-warning"></i> Alerta!</h4><span>'+errorMsg+'</span></div>');
        errorFlag = true;
      }
      if (Pass != Pass2) {
        errorMsg = 'Las contraseñas no coinciden';  
        $('#divContainerErrorMsg').append('<div id="alertDiv" class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-warning"></i> Alerta!</h4><span>'+errorMsg+'</span></div>');
        errorFlag = true;
      }
        // let errorMsg = '';  
        // $('#divContainerErrorMsg').append('<div id="alertDiv" class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-warning"></i> Alerta!</h4><span>'+errorMsg+'</span></div>');

        //Start AJAX
        if(!errorFlag){
          $.ajax({
              url : 'api/add_user',

              data : {Name:Name, Email:Email, Pass:Pass},

              type : 'POST',

              dataType : 'json',

              success : function(json) {
                if (json.error == 0) {
                  //Correct add
                  location.reload();
                  
                }else{
                  //Wrong add
                  errorMsg = json.text;
                  // $('#divContainerErrorMsg').append('<div id="alertDiv" class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-warning"></i> Alerta!</h4><span>'+errorMsg+'</span></div>');
                  errorMsg.forEach(function(e) {
                    $('#divContainerErrorMsg').append('<div id="alertDiv" class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-warning"></i> Alerta!</h4><span>'+e+'</span></div>');
                  });
                  
                }
              }
        });
        }
        //End AJAX
  })

  function suspend_client(){
    alert("AJAX upload & refresh or show error alert")
    //AJAX upload & refresh or show error alert
  }

  $('.editar_user').on('click', function(){
    let userName = $(this).attr('data-userName');
    let userEmail = $(this).attr('data-userEmail');
    let userId = $(this).attr('data-userId');

    $('#editId').val(userId);
    $('#editName').val(userName);
    $('#editEmail').val(userEmail);
    $('#modal-editUser').modal();
  })

  $('.borrar_user').on('click', function(){
    let userId = $(this).attr('data-userId');
    $('#deleteIdUser').val(userId);
    $('#modal-deleteUser').modal();
    
  })

  $('#checkEditPass').on('click',function(e){
    $('#showEditPass').toggle();
  })
  $('#ConfirmDeleteUser').on('click',function(e){
    //Guarda en una variable el id del usuario a eliminar 
   var Id = $('#deleteIdUser').val();
   //Envia por ajax ese id
   $.ajax({
                url : 'api/delete_user',

                data : {Id:Id},

                type : 'POST',

                dataType : 'json',

                success : function(json) {
                  if (json.error == 0) {
                    //Edicion correcta
                    location.reload();
                    
                  }else{
                    //Edicion incorrecta
                    errorMsg = json.text;
                    // $('#divContainerErrorMsg').append('<div id="alertDiv" class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-warning"></i> Alerta!</h4><span>'+errorMsg+'</span></div>');
                    errorMsg.forEach(function(e) {
                      $('#divContainerErrorMsgEdit').append('<div id="alertDiv" class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-warning"></i> Alerta!</h4><span>'+e+'</span></div>');
                    });
                    
                  }
                }
            });

  })

  $('#ConfirmEditUser').on('click',function(e){
    e.preventDefault();
    var Id =  $('#editId').val();
    var Name =  $('#editName').val();
    var Email =  $('#editEmail').val();
    var errorMsg = '';
    var errorFlag = false;
    if( $('#checkEditPass').prop('checked')) {
      var newPass = true;
      var Pass = $('#editPass').val();
      var Pass2 = $('#editPass2').val();
     
    }else{
      var newPass = false;
      var Pass = $('#editPass').val();
      var Pass2 = $('#editPass2').val();
      
    }

      
      
      if (Name.length == 0) {
        errorMsg = 'El nombre no puede estar vacio';  
        $('#divContainerErrorMsgEdit').append('<div id="alertDiv" class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-warning"></i> Alerta!</h4><span>'+errorMsg+'</span></div>');
        errorFlag = true;
      }
      if (Email.length == 0) {
        errorMsg = 'El E-mail no puede estar vacio';  
        $('#divContainerErrorMsgEdit').append('<div id="alertDiv" class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-warning"></i> Alerta!</h4><span>'+errorMsg+'</span></div>');
        errorFlag = true;
      }
      if (Pass.length == 0) {
        errorMsg = 'La contraseña no puede estar vacia';  
        $('#divContainerErrorMsgEdit').append('<div id="alertDiv" class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-warning"></i> Alerta!</h4><span>'+errorMsg+'</span></div>');
        errorFlag = true;
      }
      if(newPass == true){
        if (Pass.length < 8) {
          errorMsg = 'La contraseña debe tener 8 o más caracteres';  
          $('#divContainerErrorMsgEdit').append('<div id="alertDiv" class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-warning"></i> Alerta!</h4><span>'+errorMsg+'</span></div>');
          errorFlag = true;
        }
        if (Pass != Pass2) {
          errorMsg = 'Las contraseñas no coinciden';  
          $('#divContainerErrorMsgEdit').append('<div id="alertDiv" class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-warning"></i> Alerta!</h4><span>'+errorMsg+'</span></div>');
          errorFlag = true;
        }
      }

        //Start AJAX
        if(!errorFlag){
          if(newPass){
              $.ajax({
                    url : 'api/edit_user',

                    data : {Id:Id, Name:Name, Email:Email, Pass:Pass},

                    type : 'POST',

                    dataType : 'json',

                    success : function(json) {
                      if (json.error == 0) {
                        //Correct edit
                        location.reload();
                        
                      }else{
                        //Wrong edit
                        errorMsg = json.text;
                        // $('#divContainerErrorMsg').append('<div id="alertDiv" class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-warning"></i> Alerta!</h4><span>'+errorMsg+'</span></div>');
                        errorMsg.forEach(function(e) {
                          $('#divContainerErrorMsgEdit').append('<div id="alertDiv" class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-warning"></i> Alerta!</h4><span>'+e+'</span></div>');
                        });
                        
                      }
                    }
              });
          }else{
            $.ajax({
                url : 'api/edit_user',

                data : {Id:Id, Name:Name, Email:Email},

                type : 'POST',

                dataType : 'json',

                success : function(json) {
                  if (json.error == 0) {
                    //Correct edit
                    location.reload();
                    
                  }else{
                    //Wrong edit
                    errorMsg = json.text;
                    // $('#divContainerErrorMsg').append('<div id="alertDiv" class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-warning"></i> Alerta!</h4><span>'+errorMsg+'</span></div>');
                    errorMsg.forEach(function(e) {
                      $('#divContainerErrorMsgEdit').append('<div id="alertDiv" class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-warning"></i> Alerta!</h4><span>'+e+'</span></div>');
                    });
                    
                  }
                }
            });
          }
        }

  })

  $(function () {
    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' });
    $('[data-mask]').inputmask();
  })
</script>
</body>
</html>
