  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="assets/dist/img/user2-160x160.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $_SESSION['name'];  ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- sidebar menu -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MENU</li>

        <li><a href="clients"><i class="fa fa-user"></i> <span>Clientes</span></a></li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-usd"></i> <span> Facturación</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-credit-card"></i> Registrar pagos</a></li>
            <li><a href="#"><i class="fa fa-file-pdf-o"></i> Facturas</a></li>
            <li><a href="#"><i class="fa fa-bar-chart"></i> Estadísticas</a></li>
          </ul>
        </li>

        <li><a href="#"><i class="fa fa-ticket"></i> <span>Tickets</span></a></li>

      <!--Restringe la vista del link en el sidebar -->
        <?php if($_SESSION['permission'] == 1) { ?>
        <li><a href="users"><i class="fa fa-cog"></i> <span>Usuarios</span></a></li>
        <?php } ?>       
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== -->