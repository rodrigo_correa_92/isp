<?php
session_start();
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

  public function __construct() {
    parent::__construct();
	$this->load->model('Client_model');
	$this->load->model('User_model');
  }
	public function index()
	{
		header('Location: /isp');
	}

	public function login(){

    	if(isset($_POST['email']) && isset($_POST['pass'])){

			$email = $_POST['email'];
			$pass = $_POST['pass'];

			// Remove all illegal characters from email
			$email = filter_var($email, FILTER_SANITIZE_EMAIL);

			// Validate e-mail
			if (!filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
			$login = $this->User_model->check_login($email,$pass);
			$var = json_decode($login);
				if($var->error == 0){	
					$_SESSION['logged_in'] = $var->result->email;
					$_SESSION['permission'] = $var->result->permission;
					$_SESSION['name'] = $var->result->name;
					$json = array('error' => 0);
					echo json_encode( $json);
					//header('Location: /isp');

			}else{
				$json = array('error' => 1, 'text'=> $var->result);

				echo json_encode( $json);
			}
			}else{
				$json = array('error' => 1, 'text'=> "$email no es un correo válido");
				echo json_encode( $json);		
			}
			} else {
				exit('No direct script access allowed');
			}

            
    	
	}
	public function logout(){
		if (isset($_POST['logout'])) {
			session_start();
			session_destroy();
			header("location: ../login");
		}else {
			exit('Acceso restringido');
		}
		
	}
	public function add_client(){

		$Name = $_POST['Name'];
		$IdCard = $_POST['IdCard'];
		$Phone = $_POST['Phone'];
		$Email = $_POST['Email'];
		$Address = $_POST['Address'];
		$BirthDate = $_POST['BirthDate'];
		$status = 1;
		$date = date("Y-m-d H:i:s");

		$data = array(
		'name' => $_POST['Name'] ,
		'id_card' => $_POST['IdCard'] ,
		'phone' =>  $_POST['Phone'],
		'email' => $_POST['Email'] ,
		'address' => $_POST['Address'] ,
		'birth_date' => $_POST['BirthDate'] ,
		'status' => 1 ,
		'created_at' => date("Y-m-d H:i:s")
		);


		if($this->Client_model->insert_client($data)){
		$json = array('error' => 0);
		echo json_encode( $json);
		} else{
		$json = array('error' => 1, 'text'=> "Something went wrong");
		echo json_encode( $json);
		}
 
	}
	function suspend_client(){

		$data = array(
			'status' => 0
		);
		$id = $_POST['id'];

		$this->db->where('id', $id);
		$this->db->update('client', $data);
	}

	public function add_user(){

		
		$name = $_POST['Name'];
		$email = filter_var($_POST['Email'], FILTER_SANITIZE_EMAIL);
		$pass = $_POST['Pass'];
		$permission = 0;
		$status = 1;

		$errorArray = array();
		$errorFlag = false;

		//Validaciones | Validations
				//Que el mail no exista
				$query = $this->db->query("SELECT * FROM user WHERE email = ".$this->db->escape($email)."");
				$row = $query->row();
				
				if (isset($row))
				{
					$msg = 'Este mail ya existe';
					$errorFlag = true;
					array_push($errorArray,$msg);
				}
				//Que sea un mail valido
				if (filter_var($email, FILTER_VALIDATE_EMAIL) === false){
					$msg = 'El mail no tiene un formato válido';
					$errorFlag = true;
					array_push($errorArray,$msg);
				}
				//Que todos los campos tengan valores
				if(strlen($email) == 0){
					$msg = 'El mail no puede estar vacío';
					$errorFlag = true;
					array_push($errorArray,$msg);
				}
				if(strlen($name) == 0){
					$msg = 'El nombre no puede estar vacío';
					$errorFlag = true;
					array_push($errorArray,$msg);
				}
				if(strlen($pass) == 0){
					$msg = 'La contraseña no puede estar vacía';
					$errorFlag = true;
					array_push($errorArray,$msg);
				}
				if(strlen($pass ) < 8){
					$msg = 'La contraseña debe contener 8 o más caracteres';
					$errorFlag = true;
					array_push($errorArray,$msg);
				}



		//Sanitizado | Sanitize

		if($errorFlag){
			$json = array('error' => 1, 'text'=> $errorArray);
			echo json_encode( $json);
		}else{
			$encryptPass = password_hash($pass, PASSWORD_BCRYPT);
			$data = array(
			'name' => $name,
			'email' => $email,
			'pass' =>$encryptPass,
			'status' => $status,
			'Permission' => $permission,
			'created_at' => date("Y-m-d H:i:s")
			);

			if($this->User_model->add_user($data)){
			$json = array('error' => 0);
			echo json_encode( $json);
			} else{
			$json = array('error' => 1, 'text'=> "Something went wrong");
			echo json_encode( $json);
			}
		}
	}





	 	/*public function metodo_get()
		{
				
				// Prueba con GET
				
				
				if(isset($_GET['id'])){
						
							$dni = $this->Cliente_model->find_key($_GET['id']);
					if($dni)
					{
							echo "Todo ok; te devuelve un json {asdasd : asdasd}";
					}else{
							echo "No existe en db";
							echo $dni;
					}
				}else{
						echo "falta dni";
				}
			
		}
				public function metodo_post()
		{
						//Prueba con POST
				
				
				if(isset($_POST['id'])){
						
							$dni = $this->Cliente_model->find_key($_POST['id']);
					if($dni)
					{
							echo "Todo ok; te devuelve un json {asdasd : asdasd} <br>";
							var_dump ($_POST['id']);
							
					}else{
							var_dump ($_POST['id']);
							exit('No direct script access allowed');
					}
				}else{
						var_dump ($_POST['id']);
						exit('No direct script access allowed');
				}
				
		}*/ 
		public function edit_user(){

		
			$name = $_POST['Name'];
			$id = $_POST['Id'];
			$email = filter_var($_POST['Email'], FILTER_SANITIZE_EMAIL);

			if(isset($_POST['Pass'])){
				$pass = $_POST['Pass'];
			}
			

			$permission = 0;
			$status = 1;
	
			$errorArray = array();
			$errorFlag = false;
	
			//Validaciones | Validations
					//Que el mail no exista

					//Que sea un mail valido
					if (filter_var($email, FILTER_VALIDATE_EMAIL) === false){
						$msg = 'El mail no tiene un formato válido';
						$errorFlag = true;
						array_push($errorArray,$msg);
					}
					//Que todos los campos tengan valores
					if(strlen($email) == 0){
						$msg = 'El mail no puede estar vacío';
						$errorFlag = true;
						array_push($errorArray,$msg);
					}
					if(strlen($name) == 0){
						$msg = 'El nombre no puede estar vacío';
						$errorFlag = true;
						array_push($errorArray,$msg);
					}
					if(isset($pass)){
						if(strlen($pass) == 0){
							$msg = 'La contraseña no puede estar vacía';
							$errorFlag = true;
							array_push($errorArray,$msg);
						}
						if(strlen($pass ) < 8){
							$msg = 'La contraseña debe contener 8 o más caracteres';
							$errorFlag = true;
							array_push($errorArray,$msg);
						}
					}
					
	
	
	
			//Sanitizado | Sanitize
	
			if($errorFlag){
				$json = array('error' => 1, 'text'=> $errorArray);
				echo json_encode( $json);
			}else{
				if (isset($pass)) {
					$encryptPass = password_hash($pass, PASSWORD_BCRYPT);
					$data = array(
					'name' => $name,
					'email' => $email,
					'pass' =>$encryptPass,
					'updated_at' => date("Y-m-d H:i:s")
					);
				}else{
					$data = array(
					'name' => $name,
					'email' => $email,
					'updated_at' => date("Y-m-d H:i:s")
					);
				}
				
	
				if($this->User_model->edit_user($data,$id)){
				$json = array('error' => 0);
				echo json_encode( $json);
				} else{
				$json = array('error' => 1, 'text'=> "Something went wrong");
				echo json_encode( $json);
				}
			}
		}
		public function delete_user(){

			$id = $_POST['Id'];
			$data = array(
			'deleted_at' => date("Y-m-d H:i:s")
			);

			if($this->User_model->edit_user($data,$id)){
			$json = array('error' => 0);
			echo json_encode( $json);
			} else{
			$json = array('error' => 1, 'text'=> "Something went wrong");
			echo json_encode( $json);
			}
		}
	}


