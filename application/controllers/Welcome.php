<?php
defined('BASEPATH') OR exit('No direct script access allowed');
session_start();

class Welcome extends CI_Controller {

 	public function __construct() 
	{
		parent::__construct();
		$this->load->model('Client_model');
		$this->load->model('User_model');
	}
	public function index()
	{	
		if (isset($_SESSION['logged_in'])) {

			$active_clients = $this->Client_model->count_active_clients();
			$suspended_clients = $this->Client_model->count_suspended_clients();
			$all_clients = $this->Client_model->count_all_clients();
			
			$data = array(
				'active_clients' => $active_clients,
				'suspended_clients' =>  $suspended_clients,
				'all_clients' => $all_clients
			);
			$this->load->view('main', $data);

		} else {
			header("location: login");
		}
	}
	public function clients()
	{
		if (isset($_SESSION['logged_in'])) {
			$all_clientes = $this->Client_model->get_all_entries();
			$data['clients'] = $all_clientes;
			$this->load->view('clients', $data);
		} else {
			header("location: login");
		}
	}
	public function login()
	{
		if (isset($_SESSION['logged_in'])) {
			header("location: ../isp");
		} else {
			$this->load->view('login');
		}
		
	}
	public function users()
	{
		if (isset($_SESSION['logged_in'])) {

			if($_SESSION['permission'] == 1) {
				$all_users = $this->User_model->get_all_entries();
				$data['users'] = $all_users;
				$this->load->view('users', $data);
				
			}else{
				header("location: ../isp");
			}	
		} else{
			header("location: ../isp");
		}
		
	}
}

