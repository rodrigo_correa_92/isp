<?php
session_start();
defined('BASEPATH') OR exit('No direct script access allowed');

class Export extends CI_Controller {

  public function __construct() {
    parent::__construct();
	$this->load->model('Client_model');
	$this->load->model('User_model');
  }
	public function index()
	{
		header('Location: /isp');
	}

	public function users(){

        if (isset($_SESSION['logged_in'])) {

			if($_SESSION['permission'] == 1) {
				$all_users = $this->User_model->get_all_entries();
				$data['users'] = $all_users;
				$this->load->view('exports/excel_users', $data);
				
			}else{
				header("location: ../../isp");
			}	
		} else{
			header("location: ../../isp");
		}

            
	}

    
}