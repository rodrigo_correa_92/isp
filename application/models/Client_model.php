<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Client_model extends CI_Model {


  function __construct() {
    parent::__construct();
  } 
    function get_all_entries() {
      $query = $this->db->get('client');
      $results = array();
      foreach ($query->result() as $result) {
        $results[] = $result;
      }
      return $results;
    }
    function count_active_clients() {
      $val = 1;
      $query = $this->db->query("SELECT * FROM client WHERE status = ".$val." AND deleted_at IS NULL");
      $result = $query->num_rows();
      return $result;
    }
    function count_suspended_clients() {
      $val = 0;
      $query = $this->db->query("SELECT * FROM client WHERE status = ".$val." AND deleted_at IS NULL");
      $result = $query->num_rows();
      return $result;
    }      
    function count_all_clients() {
      $query = $this->db->query("SELECT * FROM client WHERE deleted_at IS NULL");
      $result = $query->num_rows();
      return $result;
    }  
    function insert_client($data){

      $result = $this->db->insert('client', $data);
      return  $result;
 
     }
     function find_key($key){
         
         $query = $this->db->query("SELECT * FROM client WHERE id_card = ".$this->db->escape($key)."");

            $row = $query->row();
            
            if (isset($row))
            {
                   return $row->id_card;

            }else{
                return false;
            }
    }

}