<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {


  function __construct() {
    parent::__construct();
  } 
    function check_login($email,$pass) {

      $answerArray = array();

      #- Find a row with that email -# 
      $query = $this->db->query("SELECT * FROM user WHERE email = ".$this->db->escape($email)."");
      $row = $query->row();
      
      #- Verify that the email exist on the DB -#
      if (isset($row))
      {
        #- Verify that the pass match -#
        if(password_verify($pass, $row->pass)){
          $answerArray['error']= 0; 
          $answerArray['result']= $row;
          $json = json_encode($answerArray);
          return $json;
        }else{
          #- Show error -#
          $error = "Contraseña incorrecta";
          $answerArray['error']= 1; 
          $answerArray['result']= $error;
          $json = json_encode($answerArray);
          return $json;
        }

      }else{
        #- Show error -#
        $error = "Este mail no existe";
        $answerArray['error']= 1; 
        $answerArray['result']= $error;
        $json = json_encode($answerArray);
        return $json;
          
      }
    }
    
    function get_all_entries() {
      $this->db->select('*');
      $this->db->from('user');
      $this->db->where('deleted_at', NULL);
      $query = $this->db->get();
      $results = array();
      foreach ($query->result() as $result) {
        $results[] = $result;
      }
      return $results;
    }
    function add_user($data){
      $result = $this->db->insert('user', $data);
      return $result;
    }

    function edit_user($data, $id){


       $this->db->set($data);
       $this->db->where('id', $id);
       $result = $this->db->update('user');
       return $result;
    }

    function delete_user($data, $id){

      $this->db->set($data);
      $this->db->where('id', $id);
      $result = $this->db->update('user');
      return $result;
      
    }
    


}